package com.ikhokha.techcheck.interfaces;

import java.util.Map;

public interface GenericComment {
  void analyze(String comment, Map<String, Integer> resultsMap, String searchTerm, String mapKey);
}
