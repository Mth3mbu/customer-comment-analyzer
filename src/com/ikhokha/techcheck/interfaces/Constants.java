package com.ikhokha.techcheck.interfaces;

public interface Constants {
  public static final String SHAKER = "Shaker";
  public static final String SHAKER_MENTIONS = "SHAKER_MENTIONS";

  public static final String MOVER = "Mover";
  public static final String MOVER_MENTIONS = "MOVER_MENTIONS";

  public static final String QUESTION = "?";
  public static final String QUESTIONS = "QUESTIONS";

  public static final String SPAM = "SPAM";
  public static final String SPAM_REGEX = "\\(?\\b(http://|https://|www[.])[-A-Za-z0-9+&amp;@#/%?=~_()|!:,.;]*[-A-Za-z0-9+&amp;@#/%=~_()|]";

  public static final String SHORTER_THAN_15 = "SHORTER_THAN_15";
}
