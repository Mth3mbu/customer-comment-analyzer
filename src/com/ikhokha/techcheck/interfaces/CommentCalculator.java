package com.ikhokha.techcheck.interfaces;

import java.util.Map;

public interface CommentCalculator {
  void incrementOccurrence(Map<String, Integer> countMap, String key);
}