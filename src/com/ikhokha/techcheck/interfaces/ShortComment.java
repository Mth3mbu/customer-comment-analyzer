package com.ikhokha.techcheck.interfaces;

import java.util.Map;

public interface ShortComment {
  void analyze(String comment, Map<String, Integer> resultsMap);
}
