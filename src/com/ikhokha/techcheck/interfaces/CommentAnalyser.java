package com.ikhokha.techcheck.interfaces;

import java.util.Map;

public interface CommentAnalyser {
  Map<String, Integer> analyze();
}
