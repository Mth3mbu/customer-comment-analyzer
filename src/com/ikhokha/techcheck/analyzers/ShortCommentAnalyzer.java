package com.ikhokha.techcheck.analyzers;

import java.util.Map;

import com.ikhokha.techcheck.interfaces.CommentCalculator;
import com.ikhokha.techcheck.interfaces.Constants;
import com.ikhokha.techcheck.interfaces.ShortComment;

public class ShortCommentAnalyzer implements ShortComment {
  private CommentCalculator commentAnalyzerCalculor;

  public ShortCommentAnalyzer(CommentCalculator calculor) {
    super();
    this.commentAnalyzerCalculor = calculor;
  }

  @Override
  public void analyze(String comment, Map<String, Integer> resultsMap) {
    if (comment.length() < 15 && !comment.isEmpty()) {
      commentAnalyzerCalculor.incrementOccurrence(resultsMap, Constants.SHORTER_THAN_15);
    }

  }

}
