package com.ikhokha.techcheck.analyzers;

import java.util.Map;

import com.ikhokha.techcheck.interfaces.CommentCalculator;
import com.ikhokha.techcheck.interfaces.GenericComment;

public class GenericCommentAnalyzer implements GenericComment {
  private CommentCalculator commentAnalyzerCalculor;

  public GenericCommentAnalyzer(CommentCalculator calculor) {
    super();
    this.commentAnalyzerCalculor = calculor;
  }

  @Override
  public void analyze(String comment, Map<String, Integer> resultsMap, String searchTerm, String mapKey) {
    String[] wordsArray = comment.toLowerCase().split(" ");
    searchTerm = searchTerm.toLowerCase();

    if (comment.toLowerCase().contains(searchTerm)) {
      for (String word : wordsArray) {
        if (word.contains(searchTerm)) {
          commentAnalyzerCalculor.incrementOccurrence(resultsMap, mapKey);
        }
      }
    }

  }

}
