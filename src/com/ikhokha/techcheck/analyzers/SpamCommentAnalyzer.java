package com.ikhokha.techcheck.analyzers;

import java.util.Map;
import java.util.regex.Pattern;

import com.ikhokha.techcheck.interfaces.CommentCalculator;
import com.ikhokha.techcheck.interfaces.Constants;
import com.ikhokha.techcheck.interfaces.SpamComment;

public class SpamCommentAnalyzer implements SpamComment {

  private CommentCalculator commentAnalyzerCalculor;

  public SpamCommentAnalyzer(CommentCalculator calculator) {
    super();
    this.commentAnalyzerCalculor = calculator;
  }

  @Override
  public void analyze(String comment, Map<String, Integer> resultsMap) {
    Pattern pattern = Pattern.compile(Constants.SPAM_REGEX);
    java.util.regex.Matcher matcher = pattern.matcher(comment);

    while (matcher.find()) {
      commentAnalyzerCalculor.incrementOccurrence(resultsMap, Constants.SPAM);
    }
  }

}
