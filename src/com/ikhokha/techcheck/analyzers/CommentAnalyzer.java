package com.ikhokha.techcheck.analyzers;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import com.ikhokha.techcheck.interfaces.CommentAnalyser;
import com.ikhokha.techcheck.interfaces.Constants;
import com.ikhokha.techcheck.interfaces.GenericComment;
import com.ikhokha.techcheck.interfaces.ShortComment;
import com.ikhokha.techcheck.interfaces.SpamComment;

public class CommentAnalyzer implements  CommentAnalyser {

	private File file;
	private ShortComment shortCommentAnalyzer;
	private SpamComment spamCommentAnalyzer;
	private GenericComment genericCommentAnalyzer;

	public CommentAnalyzer(File file,ShortComment shortCommentAnalyzer,
			SpamComment spamCommentAnalyzer, GenericComment genericCommentAnalyzer) {
		this.file = file;
		this.shortCommentAnalyzer = shortCommentAnalyzer;
		this.spamCommentAnalyzer = spamCommentAnalyzer;
		this.genericCommentAnalyzer = genericCommentAnalyzer;
	}

	
  @Override
	public synchronized Map<String, Integer> analyze() {

		Map<String, Integer> resultsMap = new HashMap<>();
		try (BufferedReader reader = new BufferedReader(new FileReader(file))) {

			String line = null;

			while ((line = reader.readLine()) != null) {
				shortCommentAnalyzer.analyze(line, resultsMap);
				genericCommentAnalyzer.analyze(line, resultsMap, Constants.MOVER, Constants.MOVER_MENTIONS);
				genericCommentAnalyzer.analyze(line, resultsMap, Constants.SHAKER, Constants.SHAKER_MENTIONS);
				genericCommentAnalyzer.analyze(line, resultsMap, Constants.QUESTION, Constants.QUESTIONS);
				spamCommentAnalyzer.analyze(line, resultsMap);
			}

		} catch (FileNotFoundException e) {
			System.out.println("File not found: " + file.getAbsolutePath());
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("IO Error processing file: " + file.getAbsolutePath());
			e.printStackTrace();
		}

		return resultsMap;
	}

}
