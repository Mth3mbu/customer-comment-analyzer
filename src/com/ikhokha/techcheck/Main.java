package com.ikhokha.techcheck;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.ikhokha.techcheck.analyzers.CommentAnalyzer;
import com.ikhokha.techcheck.analyzers.GenericCommentAnalyzer;
import com.ikhokha.techcheck.analyzers.ShortCommentAnalyzer;
import com.ikhokha.techcheck.analyzers.SpamCommentAnalyzer;
import com.ikhokha.techcheck.interfaces.CommentCalculator;

public class Main {
	public static void main(String[] args) {

		Map<String, Integer> totalResults = new HashMap<>();

		File docPath = new File("docs");
		File[] commentFiles = docPath.listFiles((d, n) -> n.endsWith(".txt"));

		int totalCpuCores = Runtime.getRuntime().availableProcessors();
		ExecutorService executorService = Executors.newFixedThreadPool(totalCpuCores);

		for (File commentFile : commentFiles) {
			executorService.execute(createService(totalResults, commentFile));
		}

		executorService.shutdown();
		printResults(totalResults);
	}

	private static void printResults(Map<String, Integer> totalResults) {
		try {
			Thread.sleep(100);
			System.out.println("RESULTS\n=======");
			totalResults.forEach((k, v) -> System.out.println(k + " : " + v));
		} catch (Exception e) {
			System.out.println("An error has occured due to:" + e.getMessage());
		}
	}

	private static CommentAnalyzerService createService(Map<String, Integer> totalResults, File commentFile) {
		CommentCalculator commentCalculator = new CommentAnalyzerCalculor();
		ShortCommentAnalyzer shortCommentAnalyzer = new ShortCommentAnalyzer(commentCalculator);
		SpamCommentAnalyzer spamCommentAnalyzer = new SpamCommentAnalyzer(commentCalculator);
		GenericCommentAnalyzer genericCommentAnalyzer = new GenericCommentAnalyzer(commentCalculator);
		CommentAnalyzer commentAnalyzer = new CommentAnalyzer(commentFile, shortCommentAnalyzer, spamCommentAnalyzer,
				genericCommentAnalyzer);

		return new CommentAnalyzerService(totalResults, commentAnalyzer);
	}
}
