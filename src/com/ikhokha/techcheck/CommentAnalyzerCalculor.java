package com.ikhokha.techcheck;

import java.util.Map;

import com.ikhokha.techcheck.interfaces.CommentCalculator;

public class CommentAnalyzerCalculor implements CommentCalculator {

  /**
   * This method increments a counter by 1 for a match type on the countMap.
   * Uninitialized keys will be set to 1
   * 
   * @param countMap the map that keeps track of counts
   * @param key      the key for the value to increment
   */
  @Override
  public void incrementOccurrence(Map<String, Integer> countMap, String key) {
    countMap.putIfAbsent(key, 0);
    countMap.put(key, countMap.get(key) + 1);
  }

}
