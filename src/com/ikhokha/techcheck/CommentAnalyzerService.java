package com.ikhokha.techcheck;

import java.util.Map;

import com.ikhokha.techcheck.interfaces.CommentAnalyser;

public class CommentAnalyzerService implements Runnable {
    private Map<String, Integer> totalResults;
    private CommentAnalyser commentAnalyzer;

    public CommentAnalyzerService(Map<String, Integer> totalResults,
            CommentAnalyser commentAnalyzer) {
        this.totalResults = totalResults;
        this.commentAnalyzer = commentAnalyzer;
    }

    @Override
    public void run() {
        Map<String, Integer> fileResults = commentAnalyzer.analyze();

        addReportResults(fileResults, totalResults);
    }

    /**
     * This method adds the result counts from a source map to the target map
     * 
     * @param source the source map
     * @param target the target map
     */
    private void addReportResults(Map<String, Integer> source, Map<String, Integer> target) {

        for (Map.Entry<String, Integer> entry : source.entrySet()) {
            target.put(entry.getKey(),
                    target.containsKey(entry.getKey()) ? target.get(entry.getKey()) + entry.getValue()
                            : entry.getValue());
        }

    }
}
